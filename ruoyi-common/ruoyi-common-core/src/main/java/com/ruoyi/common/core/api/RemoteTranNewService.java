package com.ruoyi.common.core.api;

import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.domain.SysDictDataVo;
import com.ruoyi.common.core.factor.RemoteTranNewFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * 数据字典服务
 *
 * @author ruoyi
 */
@FeignClient(contextId = "remoteTranNewService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory =
        RemoteTranNewFallbackFactory.class)
public interface RemoteTranNewService {

    @GetMapping(value = "/dict/data/getAllDictData/{dictType}")
    R<List<SysDictDataVo>> getAllDictData(@PathVariable("dictType") String dictType);


}
