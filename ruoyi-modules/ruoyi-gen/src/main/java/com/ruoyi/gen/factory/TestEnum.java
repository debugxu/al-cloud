package com.ruoyi.gen.factory;


public enum TestEnum {
    A("1", "com.ruoyi.gen.factory.TestServiceImplA"), B("2", "com.ruoyi.gen.factory.TestServiceImplB");

    private String mark;
    private String path;

    TestEnum(String mark, String path) {
        this.mark = mark;
        this.path = path;
    }

    public static String getPathByMark(String mark) {
        for (TestEnum testEnum : TestEnum.values()) {
            if (mark.equals(testEnum.getMark())) {
                return testEnum.getPath();
            }
        }
        return null;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
