package com.ruoyi.gen.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.gen.domain.GenTemplateScheme;
import com.ruoyi.gen.mapper.GenTemplateSchemeMapper;
import com.ruoyi.gen.service.IGenTemplateSchemeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 代码生成模板组管理Service业务层处理
 *
 * @author ruoyi
 * @date 2020-12-24
 */
@Service
public class GenTemplateSchemeServiceImpl extends ServiceImpl<GenTemplateSchemeMapper, GenTemplateScheme> implements IGenTemplateSchemeService {
    @Autowired
    private GenTemplateSchemeMapper genTemplateSchemeMapper;


    /**
     * 查询代码生成模板组管理列表
     *
     * @param genTemplateScheme 代码生成模板组管理
     * @return 代码生成模板组管理
     */
    @Override
    public List<GenTemplateScheme> selectGenTemplateSchemeList(GenTemplateScheme genTemplateScheme) {
        return genTemplateSchemeMapper.selectGenTemplateSchemeList(genTemplateScheme);
    }


}
