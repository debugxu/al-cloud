import request from '@/utils/request'

// 查询代码生成模板管理1列表
export function listTemplate(query) {
  return request({
    url: '/code/template/list',
    method: 'get',
    params: query
  })
}

// 查询代码生成模板管理1详细
export function getTemplate(id) {
  return request({
    url: '/code/template/' + id,
    method: 'get'
  })
}

// 新增代码生成模板管理1
export function addTemplate(data) {
  return request({
    url: '/code/template',
    method: 'post',
    data: data
  })
}

// 修改代码生成模板管理1
export function updateTemplate(data) {
  return request({
    url: '/code/template',
    method: 'put',
    data: data
  })
}

// 删除代码生成模板管理1
export function delTemplate(id) {
  return request({
    url: '/code/template/' + id,
    method: 'delete'
  })
}
